using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.Pool
{
    public class FeedbackController : MonoBehaviour
    {
        [SerializeField] private List<ParticleSystem> particlePool;
        [SerializeField] private ParticleSystem prefabToPool;
        [SerializeField] private int amountToPool;

        [SerializeField] private Transform environmentParent;

        void Start()
        {
            particlePool = new List<ParticleSystem>();
            for (int i = 0; i < amountToPool; i++)
            {
                AddParticleToPool();
            }
        }

        public ParticleSystem GetPooledObject()
        {
            for (int i = 0; i < particlePool.Count; i++)
            {
                if (!particlePool[i].gameObject.activeInHierarchy)
                {
                    return particlePool[i];
                }
            }
            AddParticleToPool();
            return particlePool[particlePool.Count-1];
        }

        public void AddParticleToPool()
        {
            ParticleSystem particleObject = Instantiate(prefabToPool, environmentParent);
            particleObject.gameObject.SetActive(false);
            particlePool.Add(particleObject);
        }

        public void ResetPool()
        {
            foreach (ParticleSystem particle in particlePool)
            {
                particle.Stop();
            }
        }

        public void SpawnParticle(Transform targetTransform)
        {
            ParticleSystem particle = GetPooledObject();
            particle.transform.position = targetTransform.position;
            particle.gameObject.SetActive(true);
            particle.Play();
        }
    }
}
