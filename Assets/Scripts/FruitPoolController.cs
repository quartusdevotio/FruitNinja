using FruitNinja.Interactable;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.Pool
{
    public class FruitPoolController : MonoBehaviour
    {
        [SerializeField] private List<FruitController> fruitPool;
        [SerializeField] private FruitController prefabToPool;
        [SerializeField] private int amountToPool;

        [SerializeField] private Transform environmentParent;

        public event Action<int, Transform> HitFruit;

        void Start()
        {
            fruitPool = new List<FruitController>();
            for (int i = 0; i < amountToPool; i++)
            {
                AddFruitToPool();
            }
        }

        private void OnDestroy()
        {
            foreach(FruitController fruit in fruitPool)
            {
                if(fruit != null)
                {
                    Destroy(fruit.gameObject);
                }
            }
            fruitPool.Clear();
            HitFruit = null;
        }

        public FruitController GetPooledObject()
        {
            for (int i = 0; i < fruitPool.Count; i++)
            {
                if (!fruitPool[i].gameObject.activeInHierarchy)
                {
                    return fruitPool[i];
                }
            }
            AddFruitToPool();
            return fruitPool[fruitPool.Count-1];
        }

        public void AddFruitToPool()
        {
            FruitController fruitObject = Instantiate(prefabToPool, environmentParent);
            fruitObject.gameObject.SetActive(false);
            fruitObject.HitFruit += OnHitFruit;
            fruitPool.Add(fruitObject);
        }

        public void ResetPool()
        {
            foreach(FruitController fruit in fruitPool)
            {
                fruit.Deactivate();
            }
        }

        private void OnHitFruit(int scoreValue, Transform fruitPosition)
        {
            HitFruit?.Invoke(scoreValue, fruitPosition);
        }
    }
}