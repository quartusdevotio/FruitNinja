using FruitNinja.Interactable;
using FruitNinja.Player;
using FruitNinja.Pool;
using FruitNinja.Spawner;
using FruitNinja.State;
using FruitNinja.UI;
using System;
using UnityEngine;

namespace FruitNinja.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private InteractableSpawner interactableSpawner;
        [SerializeField] private FeedbackController bombFeedbackController;
        [SerializeField] private FeedbackController fruitFeedbackController;
        [SerializeField] private InputController inputController;
        [SerializeField] private PausePopupController pausePopup;
        [SerializeField] private GameOverPopupController gameOverPopup;
        [SerializeField] private StartMenuController startMenu;

        private int score;
        private float timer;
        private const float StartingTime = 90f;

        private const int FirstScoreLimit = 3;
        private const int SecondScoreLimit = 7;
        private const int ThirdScoreLimit = 10;
        private const int FourthScoreLimit = 16;

        private const int FirstLimitSpawnAmount = 2;
        private const int SecondLimitSpawnAmount = 3;
        private const int ThirdLimitSpawnAmount = 5;
        private const int FourthLimitSpawnAmount = 7;
        private const int FifthLimitSpawnAmount = 10;

        private const int SecondLimitSpawnDelay = 2500;
        private const int ThirdLimitSpawnDelay = 2000;
        private const int FourthLimitSpawnDelay = 1500;
        private const int FifthLimitSpawnDelay = 1200;

        private const int FifthLimitFruitChance = 1;

        public event Action<int> ScoreChanged;
        public event Action<float> TimerChanged;

        public event Action HitBombSound;
        public event Action HitFruitSound;

        private GameState gameState = GameState.None;

        void Start()
        {
            interactableSpawner.GetFruitPool().HitFruit += OnHitFruit;
            interactableSpawner.GetBombPool().HitBomb += OnHitBomb;
            pausePopup.ContinuePressed += Pause;
            pausePopup.RestartPressed += OnRestartPressed;
            pausePopup.QuitPressed += OnQuitPressed;
            gameOverPopup.PlayAgainPressed += OnRestartPressed;
            gameOverPopup.QuitPressed += OnQuitPressed;
            startMenu.StartFruitPressed += OnStartFruitPressed;
            startMenu.QuitBombPressed += OnQuitBombPressed;

            SetTimer(StartingTime);
        }

        private void OnDestroy()
        {
            interactableSpawner.GetFruitPool().HitFruit -= OnHitFruit;
            interactableSpawner.GetBombPool().HitBomb -= OnHitBomb;
            pausePopup.ContinuePressed -= Pause;
            pausePopup.RestartPressed -= OnRestartPressed;
            pausePopup.QuitPressed -= OnQuitPressed;
            gameOverPopup.PlayAgainPressed -= OnRestartPressed;
            gameOverPopup.QuitPressed -= OnQuitPressed;
            startMenu.StartFruitPressed -= OnStartFruitPressed;
            startMenu.QuitBombPressed -= OnQuitBombPressed;
        }

        void Update()
        {
            if(gameState == GameState.Playing)
            {
                timer -= Time.deltaTime;
                TimerChanged?.Invoke(timer);
            }

            if(Input.GetKeyDown(KeyCode.Escape) && (gameState == GameState.Playing || gameState == GameState.Paused))
            {
                Pause();
            }

            if (timer <= 0)
            {
                GameOver();
            }
        }

        private void OnHitFruit(int scoreValue, Transform fruitPosition)
        {
            SetScore(score + scoreValue);
            fruitFeedbackController.SpawnParticle(fruitPosition);
            HitFruitSound?.Invoke();
        }

        private void OnHitBomb(int bombDamage, Transform bombPosition)
        {
            SetTimer(timer - bombDamage);
            bombFeedbackController.SpawnParticle(bombPosition);
            HitBombSound?.Invoke();
        }

        private void OnStartFruitPressed(Transform targetPosition)
        {
            fruitFeedbackController.SpawnParticle(targetPosition);
            interactableSpawner.Activate();
            gameState = GameState.Playing;
        }

        private void OnQuitBombPressed(Transform targetPosition)
        {
            bombFeedbackController.SpawnParticle(targetPosition);
            OnQuitPressed();
        }

        private void Pause()
        {
            if (gameState == GameState.Paused)
            {
                gameState = GameState.Playing;
                Time.timeScale = 1f;
                inputController.Activate();
                pausePopup.Hide();
            }
            else
            {
                gameState = GameState.Paused;
                Time.timeScale = 0f;
                inputController.Deactivate();
                pausePopup.Show();
            }

        }

        private void OnRestartPressed()
        {
            interactableSpawner.Deactivate();
            Time.timeScale = 1f;
            inputController.Activate();
            bombFeedbackController.ResetPool();
            fruitFeedbackController.ResetPool();
            SetScore(0);
            SetTimer(StartingTime);
            gameState = GameState.Playing;
            interactableSpawner.Activate();
        }

        private void GameOver()
        {
            interactableSpawner.Deactivate();
            Time.timeScale = 0f;
            inputController.Deactivate();
            gameOverPopup.Show();

            gameState = GameState.GameOver;
        }

        private void OnQuitPressed()
        {
            Application.Quit();
        }

        private void SetScore(int scoreValue)
        {
            score = scoreValue;
            RaiseAmountCheck();
            ScoreChanged?.Invoke(score);
        }

        private void RaiseAmountCheck()
        {
            if(score < FirstScoreLimit)
            {
                interactableSpawner.SetSpawnAmount(FirstLimitSpawnAmount);
            }
            else if(score >= FirstScoreLimit && score < SecondScoreLimit)
            {
                interactableSpawner.SetSpawnAmount(SecondLimitSpawnAmount);
                interactableSpawner.SetSpawnDelay(SecondLimitSpawnDelay);
            }
            else if(score >= SecondScoreLimit && score < ThirdScoreLimit)
            {
                interactableSpawner.SetSpawnAmount(ThirdLimitSpawnAmount);
                interactableSpawner.SetSpawnDelay(ThirdLimitSpawnDelay);
            }
            else if (score >= ThirdScoreLimit && score < FourthScoreLimit)
            {
                interactableSpawner.SetSpawnAmount(FourthLimitSpawnAmount);
                interactableSpawner.SetSpawnDelay(FourthLimitSpawnDelay);
            }
            else if (score >= FourthScoreLimit)
            {
                interactableSpawner.SetSpawnAmount(FifthLimitSpawnAmount);
                interactableSpawner.SetSpawnDelay(FifthLimitSpawnDelay);
                interactableSpawner.SetPercentage(0, FifthLimitFruitChance);
            }
        }

        private void SetTimer(float timerValue)
        {
            if (timerValue < 0f)
            {
                timer = 0f;
                return;
            }
            timer = timerValue;
        }
    }
}
