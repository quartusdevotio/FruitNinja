namespace FruitNinja.State
{
    public enum GameState
    {
        None,
        Playing,
        Paused,
        GameOver,
    };
}
