using System;
using UnityEngine;

namespace FruitNinja.Interactable
{
    public class BombController : InteractableObject, IInteractable
    {
        [SerializeField] private int bombDamage;
        public event Action<int, Transform> HitBomb;

        private void OnDestroy()
        {
            HitBomb = null;
        }

        public void OnInteracted()
        {
            Deactivate();
            HitBomb?.Invoke(bombDamage, transform);
        }
    }
}
