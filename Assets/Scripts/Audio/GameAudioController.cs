using FruitNinja.Manager;
using System;
using UnityEngine;

namespace FruitNinja.Audio
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private Sound[] soundClips;
        [SerializeField] private AudioSource soundSource;

        private void Start()
        {
            gameManager.HitFruitSound += OnHitFruit;
            gameManager.HitBombSound += OnHitBomb;
        }

        private void OnDestroy()
        {
            gameManager.HitFruitSound -= OnHitFruit;
            gameManager.HitBombSound -= OnHitBomb;
        }

        private void OnHitFruit()
        {
            PlaySound("HitFruit");
        }

        private void OnHitBomb()
        {
            PlaySound("HitBomb");
        }

        private void PlaySound(string name)
        {
            Sound sound = Array.Find(soundClips, x => x.name == name);

            if (sound != null)
            {
                soundSource.clip = sound.clip;
                soundSource.PlayOneShot(soundSource.clip);
            }
        }
    }

}
