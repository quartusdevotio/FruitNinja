using System;
using UnityEngine;

namespace FruitNinja.Audio
{
    [Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
    }
}
