using FruitNinja.Interactable;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.Pool
{
    public class BombPoolController : MonoBehaviour
    {
        [SerializeField] private List<BombController> bombPool;
        [SerializeField] private BombController prefabToPool;
        [SerializeField] private int amountToPool;

        [SerializeField] private Transform environmentParent;

        public event Action<int, Transform> HitBomb;

        void Start()
        {
            bombPool = new List<BombController>();
            for (int i = 0; i < amountToPool; i++)
            {
                AddBombToPool();
            }
        }

        private void OnDestroy()
        {
            foreach (BombController bomb in bombPool)
            {
                if (bomb != null)
                {
                    Destroy(bomb.gameObject);
                }
            }
            bombPool.Clear();
            HitBomb = null;
        }

        public BombController GetPooledObject()
        {
            for (int i = 0; i < bombPool.Count; i++)
            {
                if (!bombPool[i].gameObject.activeInHierarchy)
                {
                    return bombPool[i];
                }
            }
            AddBombToPool();
            return bombPool[bombPool.Count-1];
        }

        public void AddBombToPool()
        {
            BombController bombObject = Instantiate(prefabToPool, environmentParent);
            bombObject.gameObject.SetActive(false);
            bombObject.HitBomb += OnHitBomb;
            bombPool.Add(bombObject);
        }

        public void ResetPool()
        {
            foreach (BombController bomb in bombPool)
            {
                bomb.Deactivate();
            }
        }

        private void OnHitBomb(int bombDamage, Transform bombPosition)
        {
            HitBomb?.Invoke(bombDamage, bombPosition);
        }
    }
}
