using FruitNinja.Manager;
using TMPro;
using UnityEngine;

namespace FruitNinja.UI
{
    public class GameUIController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private TMP_Text timerText;


        void Start()
        {
            gameManager.ScoreChanged += OnScoreChanged;
            gameManager.TimerChanged += OnTimerChanged;

        }

        private void OnDestroy()
        {
            gameManager.ScoreChanged -= OnScoreChanged;
            gameManager.TimerChanged -= OnTimerChanged;
        }

        private void OnScoreChanged(int score)
        {
            scoreText.text = score.ToString();
        }

        private void OnTimerChanged(float time)
        {
            timerText.text = time.ToString("0");
        }

    }
}
