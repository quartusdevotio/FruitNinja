using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja
{
    public class RotatingObject : MonoBehaviour
    {
        [SerializeField] private float rotationSpeed;

        void Update()
        {
            gameObject.transform.localRotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0f, rotationSpeed * Time.deltaTime, 0f));
        }
    }
}
