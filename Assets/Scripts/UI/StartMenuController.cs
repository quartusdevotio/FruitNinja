using System;
using UnityEngine;

namespace FruitNinja.UI
{
    public class StartMenuController : MonoBehaviour
    {
        public event Action<Transform> StartFruitPressed;
        public event Action<Transform> QuitBombPressed;

        [SerializeField] private StartFruit startFruit;
        [SerializeField] private QuitBomb quitBomb;

        void Start()
        {
            startFruit.StartFruitPressed += OnStartFruitPressed;
            quitBomb.QuitBombPressed += OnQuitBombPressed;
        }

        private void OnDestroy()
        {
            startFruit.StartFruitPressed -= OnStartFruitPressed;
            quitBomb.QuitBombPressed -= OnQuitBombPressed;
        }

        private void OnStartFruitPressed(Transform targetTransform)
        {
            gameObject.SetActive(false);
            StartFruitPressed?.Invoke(targetTransform);
        }

        private void OnQuitBombPressed(Transform targetTransform)
        {
            QuitBombPressed?.Invoke(targetTransform);
        }
    }
}
