using FruitNinja.Interactable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.UI
{
    public class StartFruit : MonoBehaviour, IInteractable
    {
        public event Action<Transform> StartFruitPressed;

        public void OnInteracted()
        {
            StartFruitPressed?.Invoke(transform);
        }
    }
}
