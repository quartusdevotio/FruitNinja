using FruitNinja.Interactable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FruitNinja.UI
{
    public class QuitBomb : MonoBehaviour, IInteractable
    {
        public event Action<Transform> QuitBombPressed;

        public void OnInteracted()
        {
            QuitBombPressed?.Invoke(transform);
        }
    }
}
