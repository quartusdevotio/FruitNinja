using FruitNinja.Interactable;
using FruitNinja.UI;
using System;
using UnityEngine;

namespace FruitNinja.Player
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private Camera gameCamera;
        [SerializeField] private LayerMask interactableLayer;
        private bool active = true;

        private RaycastHit hit;
        private Ray rayOrigin;

        private void Update()
        {
            if (!active) return;
            rayOrigin = gameCamera.ScreenPointToRay(Input.mousePosition);
            
            if (Input.GetMouseButtonDown(0) && Physics.Raycast(rayOrigin, out hit, 100f, interactableLayer))
            {
                IInteractable interactable = hit.collider.GetComponent<IInteractable>();
                interactable.OnInteracted();
            }
        }

        public void Activate()
        {
            active = true;
        }

        public void Deactivate()
        {
            active = false;
        }
    }
}
