using System;
using UnityEngine;

namespace FruitNinja.Interactable
{
    public class FruitController : InteractableObject, IInteractable
    {
        [SerializeField] private int scoreValue;
        public event Action<int, Transform> HitFruit;

        private void OnDestroy()
        {
            HitFruit = null;
        }

        public void OnInteracted()
        {
            Deactivate();
            HitFruit?.Invoke(scoreValue, transform);
        }
    }
}
