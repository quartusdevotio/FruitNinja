using UnityEngine;

namespace FruitNinja.Interactable
{
    public class InteractableObject : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidBody;
        public Rigidbody RigidBody { get { return rigidBody; } private set { rigidBody = value; } }

        [SerializeField] private MeshRenderer meshRenderer;
        public MeshRenderer _MeshRenderer { get { return meshRenderer; } private set { meshRenderer = value; } }

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }

        public virtual void Reset()
        {
            rigidBody.velocity = Vector3.zero;
            transform.rotation = Quaternion.identity;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("FruitCatcher"))
            {
                Reset();
                Deactivate();
            }
        }
    }
}
