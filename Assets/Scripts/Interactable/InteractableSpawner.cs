using FruitNinja.Interactable;
using FruitNinja.Interactable.Type;
using FruitNinja.Pool;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

namespace FruitNinja.Spawner
{
    public class InteractableSpawner : MonoBehaviour
    {
        [SerializeField] private FruitPoolController fruitPoolController;
        [SerializeField] private BombPoolController bombPoolController;

        [SerializeField] private InteractableObject[] itemPrefab;
        [SerializeField] private Vector3 minThrowForce;
        [SerializeField] private Vector3 maxThrowForce;
        [SerializeField] private Vector3 minSpawnPosition;
        [SerializeField] private Vector3 maxSpawnPosition;
        [SerializeField] private Vector3 minRotation;
        [SerializeField] private Vector3 maxRotation;

        [SerializeField] private int amountToSpawn;
        [SerializeField] private float[] percentages;

        [SerializeField] private int minSpawnRate = 200;
        [SerializeField] private int maxSpawnRate = 350;
        [SerializeField] private int spawnRoundDelay = 3000;

        [SerializeField] private Material[] fruitMaterials;

        private bool active = false;
        private bool spawningDone = true;
        private CancellationTokenSource cancellationTokenSource;

        private Task task;

        private void Start()
        {
            cancellationTokenSource = new CancellationTokenSource();
        }

        private async void Update()
        {
            if (!active) return;
            if (!spawningDone) return;

            var cancellationToken = cancellationTokenSource.Token;
            await SpawnInteractable(cancellationToken);
        }

        private async Task SpawnInteractable(CancellationToken cancellationToken) //coba ganti ke task
        {
            spawningDone = false;
            InteractableObject item;
            for(int i=0; i<amountToSpawn; i++)
            {
                if (RandomSpawn() == (int)InteractableType.Fruit)
                {
                    item = fruitPoolController.GetPooledObject();
                    item._MeshRenderer.material = fruitMaterials[Random.Range(0, fruitMaterials.Length)];
                }
                else
                {
                    item = bombPoolController.GetPooledObject();
                }
                if (item == null) return;
                item.Activate();
                item.Reset();
                item.transform.position = new Vector3(Random.Range(minSpawnPosition.x, maxSpawnPosition.x),
                    Random.Range(minSpawnPosition.y, maxSpawnPosition.y), Random.Range(minSpawnPosition.z, maxSpawnPosition.z));
                item.RigidBody.AddForce(new Vector3(Random.Range(minThrowForce.x, maxThrowForce.x),
                    Random.Range(minThrowForce.y, maxThrowForce.y), Random.Range(minThrowForce.z, maxThrowForce.z)), ForceMode.Impulse);
                item.transform.Rotate(new Vector3(Random.Range(minRotation.x, maxRotation.x),
                    Random.Range(minRotation.y, maxRotation.y), Random.Range(minRotation.z, maxRotation.z)));
                await Task.Delay(Random.Range(minSpawnRate, maxSpawnRate+1), cancellationToken);
            }
            await Task.Delay(spawnRoundDelay, cancellationToken);
            spawningDone = true;
        }

        private int RandomSpawn()
        {
            float random = Random.Range(0f, 1f);
            float total = 0f;
            float addNum = 0f;

            for (int i = 0; i < percentages.Length; i++)
            {
                total += percentages[i];
            }

            for (int i = 0; i < itemPrefab.Length; i++)
            {
                if (percentages[i] / total + addNum >= random)
                {
                    return i;
                }
                else
                {
                    addNum += percentages[i] / total;
                }
            }
            return 0;
        }

        public FruitPoolController GetFruitPool()
        {
            return fruitPoolController;
        }

        public BombPoolController GetBombPool()
        {
            return bombPoolController;
        }

        public void SetSpawnAmount(int amount)
        {
            amountToSpawn = amount;
        }

        public void SetSpawnDelay(int delay)
        {
            spawnRoundDelay = delay;
        }

        public void SetPercentage(int index, int chance)
        {
            percentages[index] = chance;
        }

        public void Activate()
        {
            active = true;
            spawningDone = true;
        }

        public void Deactivate()
        {
            active = false;
            if(cancellationTokenSource != null)
            {
                CancelTaskToken();
            }
            fruitPoolController.ResetPool();
            bombPoolController.ResetPool();
        }

        private void OnDisable()
        {
            if (cancellationTokenSource != null)
            {
                CancelTaskToken();
            }
        }

        private void CancelTaskToken()
        {
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();
            cancellationTokenSource = new CancellationTokenSource();
        }
    }
}
