namespace FruitNinja.Interactable
{
    public interface IInteractable
    {
        void OnInteracted();
    }
}
