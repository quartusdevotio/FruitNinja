namespace FruitNinja.Interactable.Type
{
    public enum InteractableType
    {
        Fruit = 0,
        Bomb = 1,
        StartFruit = 2,
        QuitBomb = 3,
    };
}
